package net.genkiflare.tabula;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

public class Rasa {

	// Generate some amount of Simplex Noise using the modified SimplexNoise Class

	public static float[][] generateSimplexNoise(int width, int height){
		float[][] simplexnoise = new float[width][height];
		float frequency = 5.0f / (float) width;

		Random r = new Random();
		double ri = r.nextDouble();

		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				simplexnoise[x][y] = (float) SimplexNoise.noise(x * frequency,y * frequency);
				simplexnoise[x][y] = (simplexnoise[x][y] + 1) / 2;   //generate values between 0 and 1
			}
		}

		return simplexnoise;
	}

	/*
	 * Get an int representing an RGB colour
	 * Red, Green, Blue - Integer values ranging from 0-255
	 */
	public static int getIntFromColor(int Red, int Green, int Blue) {
		Red = (Red << 16) & 0x00FF0000;
		Green = (Green << 8) & 0x0000FF00;
		Blue = Blue & 0x000000FF;

		return 0xFF000000 | Red | Green | Blue;
	}

	public static void main(String[] args) {

		// Settings

		int dim = 1000;
		int maxOctave = 4;
		BufferedImage img = new BufferedImage(dim, dim, BufferedImage.TYPE_INT_ARGB);
		BufferedImage comb = new BufferedImage(dim, dim, BufferedImage.TYPE_INT_ARGB);
		BufferedImage colr = new BufferedImage(dim, dim, BufferedImage.TYPE_INT_ARGB);
		Graphics gfx = comb.getGraphics();

		float[][] noise = generateSimplexNoise(dim, dim);

		// For each Octave, loop through the pixels and generate some Simplex noise using the permutation table

		for (int o = 1; o <= maxOctave; o++) {
			for (int x = 0; x < dim; x++) {
				for (int y = 0; y < dim; y++) {
					float ran = noise[x][y];

					int i = (int) (ran * 255);

					Color white = Color.WHITE;

					int r, g, b, a;
					r = i;
					g = i;
					b = i;
					a = 255;

					int col = (a << 24) | (r << 16) | (g << 8) | b;

					img.setRGB(x, y, col);
				}
			}

			System.out.println("Generating Octave: "+o);

			BufferedImage before = img;
			int w = before.getWidth();
			int h = before.getHeight();
			AffineTransform at = new AffineTransform();
			at.scale(o, o);
			AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
			comb = scaleOp.filter(before, comb);
		}

		// Convert the grayscale image into a HSB coloured image

		for (int x = 0; x < dim; x++) {
			for (int y = 0; y < dim; y++) {

				int rgb = comb.getRGB(x, y);
				int r = (rgb >> 16) & 0xFF;
				int g = (rgb >> 8) & 0xFF;
				int b = (rgb & 0xFF);
				float f = (float)r/255;
				Color col = Color.getHSBColor(f, 1.0F, 0.5F);
				// System.out.println("Hues: "+r+" "+g+" "+b+" COLOR: "+col.getRed()+"."+col.getGreen()+"."+col.getBlue());
				colr.setRGB(x, y, getIntFromColor(col.getRed(), col.getGreen(), col.getBlue()));
			}
		}

		// Generate a terrain map

		BufferedImage tmap = new BufferedImage(dim, dim, BufferedImage.TYPE_INT_ARGB);

		Color cw = Color.getHSBColor((192F/360F), (25F/100F), (92F/100F));
		Color cs = Color.getHSBColor((62F/360F), (23F/100F), (77F/100F));
		Color cl = Color.getHSBColor((125F/360F), (34F/100F), (77F/100F));
		Color cm = Color.getHSBColor((74F/360F), (36F/100F), (71F/100F));

		for (int x = 0; x < dim; x++) {
			for (int y = 0; y < dim; y++) {
				int rgb = comb.getRGB(x, y);
				int r = (rgb >> 16) & 0xFF;
				int g = (rgb >> 8) & 0xFF;
				int b = (rgb & 0xFF);
				float rf = (float)((float)r/(float)255);
				System.out.println(rf);
				if(rf<0.1F) {
					tmap.setRGB(x, y, getIntFromColor(cw.getRed(), cw.getGreen(), cw.getBlue()));
				}
				if (rf>=0.1F && rf<0.4F) { 
					tmap.setRGB(x, y, getIntFromColor(cs.getRed(), cs.getGreen(), cs.getBlue()));
				}
				if (rf>=0.4F && rf<0.8F) { 
					tmap.setRGB(x, y, getIntFromColor(cl.getRed(), cl.getGreen(), cl.getBlue()));
				}
				if (rf>=0.8F && rf<=1.0F) { 
					tmap.setRGB(x, y, getIntFromColor(cm.getRed(), cm.getGreen(), cm.getBlue()));
				}
			}
		}

		File f = new File("C://out/tt.png");
		File f2 = new File("C://out/tt2.png");
		try {
			f.mkdirs();
			ImageIO.write(colr, "png", f);
			ImageIO.write(tmap, "png", f2);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}